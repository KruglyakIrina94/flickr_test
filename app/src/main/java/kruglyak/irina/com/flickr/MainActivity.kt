package kruglyak.irina.com.flickr

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import java.io.InputStream
import android.graphics.BitmapFactory
import android.util.Log
import android.widget.Toast


class MainActivity : AppCompatActivity() {

    companion object {

        fun newIntent(context: Context): Intent {
            val intent = Intent(context, MainActivity::class.java)
            return intent

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val assetManager = assets
        var pictureList = ArrayList<Pictures>()
        var searchHistory = ArrayList<String>()

        // load image
        val pictures: Array<String> = getAssets().list("Photos")


        for (item: String in pictures) {
            val inputStream: InputStream = assetManager.open("Photos/"+item)
            val bitmap = BitmapFactory.decodeStream(inputStream)
            pictureList.add(Pictures(item, bitmap))
        }

        // Creates a vertical Layout Manager
        rv_recent_contacts.layoutManager = GridLayoutManager(this, 4)
        rv_recent_contacts.adapter = PicturesAdapter(pictureList, this, { partItem : Pictures -> partItemClicked(partItem) })

        btn_search.setOnClickListener {
            var searchHint: String = ""

            searchHistory.add(search_view.query.toString())

            for(search: String in searchHistory){

                searchHint += search + " "
            }
            search_view.setQueryHint(searchHint)

            if(search_view.query.equals("")){
                // Access the RecyclerView Adapter and load the data into it
                rv_recent_contacts.adapter = PicturesAdapter(pictureList, this, { partItem : Pictures -> partItemClicked(partItem) })
            } else{
                var newArr = ArrayList<Pictures>()
                for(pics: Pictures in pictureList){
                        if (pics.name?.contains(search_view.query)) {
                            newArr.add(pics);
                        }
                }
                rv_recent_contacts.adapter = PicturesAdapter(newArr, this, { partItem : Pictures -> partItemClicked(partItem) })
            }
        }
    }

    private fun partItemClicked(partItem : Pictures) {
        val intent = FullScreenImage.newIntent(this, partItem.image)
        startActivity(intent)
    }
}
