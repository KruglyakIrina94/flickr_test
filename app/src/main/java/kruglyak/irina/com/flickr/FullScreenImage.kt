package kruglyak.irina.com.flickr

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_full_screen_image.*

/**
 * Created by biinnovation on 02.08.2018.
 */
class FullScreenImage : AppCompatActivity() {


    companion object {
        var imageBitmap = ArrayList<Bitmap>()


        fun newIntent(context: Context, image: Bitmap): Intent {

            val intent = Intent(context, FullScreenImage::class.java)
            imageBitmap.clear()
            imageBitmap.add(image)
            return intent

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_screen_image)
        fullImage.setImageBitmap(imageBitmap.get(0))
    }
}