package kruglyak.irina.com.flickr

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.picture_list_item.view.*

/**
 * Created by biinnovation on 31.07.2018.
 */
class PicturesAdapter(val items : ArrayList<Pictures>, val context: Context, val clickListener: (Pictures) -> Unit): RecyclerView.Adapter<ViewHolder>() {

    // Gets the number of animals in the list
    override fun getItemCount(): Int {
        return items.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.picture_list_item, parent, false))
    }

    // Binds each item in the ArrayList to a view
    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        (holder as ViewHolder).bind(items[position], clickListener)
    }
}

class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    fun bind(part: Pictures, clickListener: (Pictures) -> Unit) {
        itemView.pic_name.text = part.name
        itemView.pic_image.setImageBitmap(part.image)
        itemView.setOnClickListener { clickListener(part)}
    }
}