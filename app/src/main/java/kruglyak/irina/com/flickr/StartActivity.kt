package kruglyak.irina.com.flickr

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.Button
import android.widget.ImageView

/**
 * Created by biinnovation on 31.07.2018.
 */
class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
        // get reference to button
        val btn_click_me = findViewById<ImageView>(R.id.bt_start) as ImageView
        // set on-click listener
        btn_click_me.setOnClickListener {
            val intent = MainActivity.newIntent(this)
            startActivity(intent)
        }
    }
}