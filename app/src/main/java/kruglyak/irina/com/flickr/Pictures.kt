package kruglyak.irina.com.flickr

import android.graphics.Bitmap
import android.graphics.drawable.Drawable

/**
 * Created by biinnovation on 31.07.2018.
 */

 class Pictures {
    var name: String
    var image: Bitmap

    constructor(name: String, image: Bitmap) {
        this.name = name
        this.image = image
    }
}